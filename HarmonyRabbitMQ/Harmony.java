import com.rabbitmq.client.*;
import java.util.Scanner;

public class Harmony {
    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        String username = argv.length == 1 ? argv[0] : "Anonymous";

        try (
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            Scanner scanner = new Scanner(System.in);
        ) {
            channel.exchangeDeclare("harmony", "fanout");

            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, "harmony", "");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                String sender = message.substring(0, message.indexOf(">"));
                if (!sender.equals(username)) {
                    System.out.println(message);
                }
            };
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {});

            while (true) {
                String message = username + "> " + scanner.nextLine();
                channel.basicPublish("harmony", "", null, message.getBytes());
            }
        }
    }
}