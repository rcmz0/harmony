import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;

public class Server {

    public static void main(String[] args) {
        try {
            ChatServiceImpl instance = new ChatServiceImpl();
            ChatService_itf server = (ChatService_itf) UnicastRemoteObject.exportObject(instance, 0);

            Registry registry = LocateRegistry.getRegistry();
            registry.bind("Harmony", server);

            System.out.println("Server ready");
        } catch (Exception e) {
            System.err.println("Error on server :" + e);
            e.printStackTrace();
        }

    }
}
